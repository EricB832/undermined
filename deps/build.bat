cd glew-1.10.0
echo "Building GLEW..."
call build
copy lib ..\..\lib
copy bin ..\..\bin
cd ..\glfw-3.0.3
echo "Building GLFW..."
call build
copy build\src\libglfw3.a ..\..\lib
cd ..\SOIL
echo "Building SOIL..."
call build
copy lib\libSOIL.a ..\..\lib
cd ..\yaml-cpp-0.5.1
echo "Building yaml-cpp..."
call build
copy build\libyaml-cpp.a ..\..\lib
cd ..\freetype-2.5.0.1
echo "Building freetype..."
call build
copy objs\freetype.a ..\..\lib\libfreetype.a
cd ..\ftgl-2.1.3~rc5\src
echo "Building FTGL..."
mingw32-make
copy libftgl.a ..\..\..\lib
cd ..\..

echo "Cleaning up..."
.\cleanup.sh
echo "Done! Press any key to continue..."
pause