if not exist build mkdir build
cd build
cmake -G "MinGW Makefiles" -DBUILD_SHARED_LIBS=OFF -DBoost_INCLUDE_DIR=../../include ..
mingw32-make.exe
cd ..