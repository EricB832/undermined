if not exist bin mkdir bin
if not exist lib mkdir lib
gcc -DGLEW_NO_GLU -O2 -Wall -W -Iinclude  -DGLEW_BUILD -o src/glew.o -c src/glew.c
gcc -shared -Wl,-soname,glew32.dll -Wl,--out-implib,lib/libglew32.a -o bin/glew32.dll src/glew.o -L/mingw/lib -lglu32 -lopengl32 -lgdi32 -luser32 -lkernel32

:: Create library file: lib/libglew32.a
ar cr lib/libglew32.a src/glew.o
