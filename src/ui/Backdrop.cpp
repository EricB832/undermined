#include "Backdrop.h"
#include "Background.h"

using namespace std;

Backdrop::Backdrop(string file):Painter(file, "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"){
    strokes.emplace_back(new Background);
}
