/*
 * Button.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#include "Button.h"
#include "../Options.h"
#include "../rendering/Painter.h"
#include "../screens/Screen.h"
#include <yaml-cpp/yaml.h>
#include <string>
#include <algorithm>
#include <vector>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
using namespace std;


const std::string root = "Buttons";
const std::string text_color = "Text Color";

Button::Button(std::string button, FTFont& font, std::function<void()> activate):
        Element(file[root][button]),
        activate(activate),
        state(0),
        font(font),
        idled(file[root][button]["Idle"]["Texture"], file["Width"].as<int>(), file["Height"].as<int>()),
        highlighted(file[root][button]["Highlight"]["Texture"], file["Width"].as<int>(), file["Height"].as<int>()),
        pressed(file[root][button]["Press"]["Texture"], file["Width"].as<int>(), file["Height"].as<int>()){
    auto node = file[root][button];
    text = node["Text"].as<std::string>();
    r = node[text_color]["Red"].as<float>();
    g = node[text_color]["Green"].as<float>();
    b = node[text_color]["Blue"].as<float>();
}

void Button::idle(){
	state = 0;
}

void Button::highlight() {
	state = 1;
}

void Button::press(){
	state = 2;
}

vector<GLfloat> Button::vertices() const{
	return position.vertices();
}

vector<GLfloat> Button::coordinates() const{
	switch(state){
	case 1: 		 return highlighted.coordinates();
	case 2: 		 return pressed.coordinates();
	case 0: default: return idled.coordinates();
	}
}

void Button::print() const{
    font.FaceSize(20);
	FTBBox dimensions = font.BBox(text.c_str());
    float x = position.get(0).x + (position.width() - (dimensions.Upper().X() - dimensions.Lower().X()))/2;
    float y = position.get(0).y + (position.height() - (dimensions.Upper().Y() - dimensions.Lower().Y()))/2;

	glColor3d(r,g,b);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
    glOrtho(0,Screen::Width,0,Screen::Height,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	font.Render(text.c_str(), -1, FTPoint(x,y));

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

}
