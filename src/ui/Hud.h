#ifndef HUD_H
#define HUD_H

#include "../rendering/Painter.h"
#include <glm/glm.hpp>
class Player;

class Hud : public Painter {
private:
    Player& player;
    int health_bar;
    void refill();
public:
    static glm::mat4 Camera;

    Hud(Player& player);
    void update();
};

#endif // HUD_H
