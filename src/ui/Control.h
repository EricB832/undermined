/*
 * Control.h
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#ifndef CONTROL_H_
#define CONTROL_H_

#include <memory>
#include <vector>
class Control {
private:
	bool press(int key);
	bool hold();
	bool release(int key);

	int key;
	std::function<void()> start;
	std::function<void()> act;
	std::function<void()> end;

	bool started;
public:
    Control(int key, std::function<void()> start);
	Control(int key, std::function<void()> start, std::function<void()> act, std::function<void()> end);
	void handle();

};

#endif /* CONTROL_H_ */
