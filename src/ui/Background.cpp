#include "Background.h"
#include "../screens/Screen.h"

using namespace std;

Background::Background():position(0, 0, Screen::Width, Screen::Height){}

vector<float> Background::coordinates() const {
     return {0,0,1,0,0,1,1,0,1,1,0,1};
}

vector<float> Background::vertices() const {
    return position.vertices();
}
