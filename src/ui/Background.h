#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "../rendering/Stroke.h"
#include "../rendering/Sprite.h"
#include "../rendering/Quad.h"

class Background : public Stroke {
protected:
    Quad position;
public:
    Background();
    std::vector<float> vertices() const;
    std::vector<float> coordinates() const;
};

#endif // BACKGROUND_H
