#include "Control.h"
#include "Keys.h"
#include "../floor/Floor.h"
#include "Menu.h"
#include "../characters/Player.h"
#include <Gl/glfw3.h>
#include "../Game.h"
using namespace std;
Control::Control(int key, function<void()> start, function<void()> act, function<void()> end):key(key),start(start),act(act),end(end), started(false){}
Control::Control(int key, function<void()> start):Control(key, start, nullptr, nullptr){}

void Control::handle(){
	if (press(key)){
		if(start) start();
	} else if (release(key)){
		if(end) end();
	} else if (hold()){
		if(act) act();
	}
}

bool Control::press(int key) {
    if(glfwGetKey(Game::window, key) && Keys::press(key)) return started = true;
	return false;
}

bool Control::release(int key) {
    if(!glfwGetKey(Game::window, key) && Keys::release(key)) return !(started = false);
	return false;
}

bool Control::hold(){
	return started;
}
