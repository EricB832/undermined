/*
 * Panel.cpp
 *
 *  Created on: Mar 17, 2013
 *      Author: Eric
 */

#include "HudElement.h"
using namespace std;

HudElement::HudElement(Quad position, string hud_element):
    Element(position),
    sprite(file[hud_element]["Texture"], file["Width"].as<int>(), file["Height"].as<int>()) {

}

vector<float> HudElement::coordinates() const{
	return sprite.coordinates();
}
