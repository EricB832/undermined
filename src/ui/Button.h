/*
 * Button.h
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include <FTGL/ftgl.h>
#include <string>
#include <functional>
#include "Element.h"
#include "../rendering/Sprite.h"
#include "../rendering/Quad.h"
class Button : public Element{
public:
	Button(std::string button, FTFont& font, std::function<void()> activate);
	std::vector<float> vertices() const;
	std::vector<float> coordinates() const;
	void render() const;
    void idle();
    void highlight();
    void press();
	void print() const;

	std::function<void()> activate;
	std::string text;
private:
	void copy(const Button& b);
	float r, g, b;
	int state;

	FTFont& font;
	Sprite idled;
	Sprite highlighted;
	Sprite pressed;

};

#endif /* BUTTON_H_ */
