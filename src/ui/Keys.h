/*
 * Keys.h
 *
 *  Created on: Mar 5, 2013
 *      Author: eric
 */

#ifndef KEYS_H_
#define KEYS_H_

#include <map>

class Keys {
public:
	static bool press(int key);
	static bool release(int key);
private:
	static std::map<int, bool> keys;
};

#endif /* KEYS_H_ */
