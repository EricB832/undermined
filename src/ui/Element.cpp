/*
 * Element.cpp
 *
 *  Created on: Mar 17, 2013
 *      Author: Eric
 */

#include "Element.h"
using namespace std;
const YAML::Node Element::file = YAML::LoadFile("data/info/ui.yml");

Element::Element(const YAML::Node& node):position(node["Position"]) {}
Element::Element(Quad position):position(position){}
vector<float> Element::vertices() const{
	return position.vertices();
}
