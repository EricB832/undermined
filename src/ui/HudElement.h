/*
 * Panel.h
 *
 *  Created on: Mar 17, 2013
 *      Author: Eric
 */

#ifndef PANEL_H_
#define PANEL_H_

#include "Element.h"
#include "../rendering/Sprite.h"
#include "../rendering/Quad.h"
#include <string>

class HudElement : public Element{
public:
    HudElement(Quad position, std::string name);
	std::vector<float> coordinates() const;
private:
	Sprite sprite;
};

#endif /* PANEL_H_ */
