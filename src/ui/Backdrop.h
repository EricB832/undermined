#ifndef BACKDROP_H
#define BACKDROP_H

#include <string>
#include "../rendering/Painter.h"

class Backdrop : public Painter
{
public:
    Backdrop(std::string file);
};

#endif // BACKDROP_H
