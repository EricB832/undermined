#include "Hud.h"
#include "Element.h"
#include "../characters/Player.h"
#include "../screens/Screen.h"
#include "HudElement.h"
#include <glm/gtc/matrix_transform.hpp>

glm::mat4 Hud::Camera = glm::ortho<GLfloat>(0, Screen::Width, 0, Screen::Height);

Hud::Hud(Player& player):Painter("data/art/ui.png", "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"),player(player){
    refill();
}

void Hud::update(){
    if(health_bar != player.health){
        strokes.clear();
        refill();
    }

}

void Hud::refill(){
    health_bar = player.health;
    for(int i = 0; i < health_bar; i++){
        strokes.emplace_back(new HudElement(Quad(Screen::Width-300+((i%10)*30), 870-30*(i/10), 24, 24), "Heart"));
    }
}
