/*
 * Element.h
 *
 *  Created on: Mar 17, 2013
 *      Author: Eric
 */

#ifndef ELEMENT_H_
#define ELEMENT_H_

#include "../rendering/Stroke.h"
#include "../rendering/Quad.h"
#include <yaml-cpp/yaml.h>
#include <vector>
#include <string>

class Element : public Stroke {
public:
    Element(const YAML::Node& node);
    Element(Quad position);
	std::vector<float> vertices() const;
    void update(){}
	Quad position;
protected:
	static const YAML::Node file;
};

#endif /* ELEMENT_H_ */
