#include "wall.h"

using namespace std;

Wall::Wall(glm::vec2 p1, glm::vec2 p2):p1(p1), p2(p2){}

Quad Wall::quad(){
    float x = min(p1.x, p2.x);
    float w = max(p1.x, p2.x) - x;
    float y = min(p1.y, p2.y);
    float h = max(p1.y, p2.y) - y;
    return Quad(x, y, w, h);
}
