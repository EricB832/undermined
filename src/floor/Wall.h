#ifndef WALL_H
#define WALL_H

#include <glm/glm.hpp>
#include "../rendering/Quad.h"

class Wall {
public:
    Wall(glm::vec2 p1, glm::vec2 p2);
    Quad quad();
    glm::vec2 p1, p2;
};

#endif // WALL_H
