/*
 * Floor.cpp
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#include "Floor.h"
#include "../Game.h"
#include <stdexcept>
#include <string>
#include <memory>
#include <stdexcept>
#include <glm/gtc/matrix_transform.hpp>
using namespace std;

Floor::Floor(int w, int h):Painter(	"data/art/tiles.png", "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"),width(w),height(h){
	for(int i = 0; i < w*h; i++){
        int dirt = rand() % 5 + 1;
        string tile = "Dirt " + to_string(dirt);
        strokes.emplace_back(new Square(i%w, i/w, unique_ptr<Tile>(new Tile(tile))));
	}
    scroll_speed = 25.0;

    walls = {
        Wall(glm::vec2(0, 0), glm::vec2(width*Square::width, 0)),
        Wall(glm::vec2(width*Square::width, 0), glm::vec2(width*Square::width, height*Square::height)),
        Wall(glm::vec2(width*Square::width, height*Square::height), glm::vec2(0, height*Square::height)),
        Wall(glm::vec2(0, height*Square::height), glm::vec2(0,0)),
    };
}

bool Floor::valid(int i, int j){
	if(i >= 0 && j >= 0 && i < width && j < height){
		return true;
    }
	return false;
}

Square& Floor::get(int i, int j){
    if(valid(i,j)){
		return *dynamic_cast<Square*>(strokes[j*width+i].get());
    }
    throw std::runtime_error("Floor square out of bounds.");
}
