/*
 * Furniture.h
 *
 *  Created on: Mar 14, 2013
 *      Author: Eric
 */

#ifndef FURNITURE_H_
#define FURNITURE_H_

#include <string>
#include <vector>
#include <memory>
#include <yaml-cpp/yaml.h>
#include "../rendering/Sprite.h"
#include "../rendering/Quad.h"

class Furniture {
protected:
    static const YAML::Node file;
public:
    Furniture(const YAML::Node& node);

	Sprite sprite;
};

#endif /* FURNITURE_H_ */
