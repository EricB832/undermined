/*
 * Tile.h
 *
 *  Created on: Mar 16, 2013
 *      Author: Eric
 */

#ifndef TILE_H_
#define TILE_H_

#include "Furniture.h"
#include <string>

class Tile: public Furniture {
public:
	Tile(std::string tile);
};

#endif /* TILE_H_ */
