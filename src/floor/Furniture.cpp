/*
 * Furniture.cpp
 *
 *  Created on: Mar 14, 2013
 *      Author: Eric
 */

#include "Furniture.h"
#include "../floor/Square.h"
#include <string>
using namespace std;

const YAML::Node Furniture::file = YAML::LoadFile("data/info/tiles.yml");

Furniture::Furniture(const YAML::Node& node):
        sprite(node["Texture"], file["Width"].as<int>(), file["Height"].as<int>()){

}
