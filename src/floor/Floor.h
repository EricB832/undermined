/*
 * Floor.h
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#ifndef FLOOR_H_
#define FLOOR_H_

#include "Square.h"
#include "../rendering/Painter.h"
#include "../floor/Wall.h"
#include "../Error.h"

class Square;
class Indicator;

class Floor : public Painter{
public:
    Floor(int w, int h);
	Square& get(int i, int j);
	bool valid(int i, int j);
    int width;
    int height;
    float scroll_speed;
    std::vector<Wall> walls;
};

#endif /* FLOOR_H_ */
