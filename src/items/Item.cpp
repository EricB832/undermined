/*
 * Item.cpp
 *
 *  Created on: Jun 8, 2013
 *      Author: Eric
 */

#include "Item.h"
#include <glm/gtc/matrix_transform.hpp>
using namespace std;

const string path = "data/info/items.yml";
YAML::Node Item::file = YAML::LoadFile(path);

Item::Item(Quad position, const YAML::Node& node):
        matrix(glm::mat4()),
        node(node),
        position(position),
        sprite(node["Texture"], file["Width"].as<int>(), file["Height"].as<int>()){

}

Item::Item(float x, float y, const YAML::Node& node):
        matrix(glm::mat4()),
        node(node),
        position(x,y,0,0),
        sprite(node["Texture"], file["Width"].as<int>(), file["Height"].as<int>()){
    float w = node["Size"]["Width"].as<int>();
    float h = node["Size"]["Height"].as<int>();
    position = Quad(x, y, w, h);
}

vector<float> Item::vertices() const{
    return position.pre_multiply(matrix).vertices();
}

vector<float> Item::coordinates() const{
    return sprite.coordinates();
}

void Item::rotate_about(glm::vec2 point, float degrees){
    matrix = glm::translate<GLfloat>(glm::mat4(), glm::vec3(point.x, point.y, 0));
    matrix = glm::rotate<GLfloat>(matrix, degrees, glm::vec3(0,0,-1));
    matrix = glm::translate<GLfloat>(matrix, glm::vec3(-point.x,-point.y,0));
}

float Item::x() const {
    return position.get(0).x;
}

float Item::y() const {
    return position.get(0).y;
}

void Item::reload(){
    float w = node["Size"]["Width"].as<int>();
    float h = node["Size"]["Height"].as<int>();
    position = Quad(x(), y(), w, h);
}

void Item::reload_yaml(){
    file = YAML::LoadFile(path);
}
