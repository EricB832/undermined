#ifndef WEAPON_H
#define WEAPON_H

#include "Item.h"
#include <string>

class Weapon : public Item
{
public:
    Weapon(float x, float y, std::string name);
};

#endif // WEAPON_H
