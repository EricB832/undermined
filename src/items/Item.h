/*
 * Item.h
 *
 *  Created on: Jun 8, 2013
 *      Author: Eric
 */

#ifndef ITEM_H_
#define ITEM_H_

#include "../rendering/Stroke.h"
#include "../rendering/Quad.h"
#include "../rendering/Sprite.h"
#include <string>
#include <yaml-cpp/yaml.h>

class Item : public Stroke{
private:
    glm::mat4 matrix;
protected:
    static YAML::Node file;
    const YAML::Node& node;
public:
    Item(Quad position, const YAML::Node& node);
    Item(float x, float y, const YAML::Node& node);

    Quad position;
    Sprite sprite;

    std::vector<float> vertices() const;
    std::vector<float> coordinates() const;

    // Movement/Positioning
    void rotate_about(glm::vec2 point, float degrees);
    float x() const;
    float y() const;

    // Dev/Debug crap
    void reload();

    // Static dev/debug crap
    static void reload_yaml();
};

#endif /* ITEM_H_ */
