/*
 * Characters.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef CHARACTERS_H_
#define CHARACTERS_H_

#include "../rendering/Painter.h"
#include "Player.h"
#include <vector>
#include <string>

class Enemy;

class Characters : public Painter{
public:
    Characters();
	Player& player();
    std::vector<Enemy*> enemies() const;
};

#endif /* CHARACTERS_H_ */
