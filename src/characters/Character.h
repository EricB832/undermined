/*
 * Character.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef CHARACTER_H_
#define CHARACTER_H_

#include "../rendering/Sprite.h"
#include "../rendering/Quad.h"
#include "../rendering/Stroke.h"
#include "../rendering/Animations.h"
#include <glm/glm.hpp>
#include <map>

enum Direction {
	UP=1,
	RIGHT=2,
	DOWN=4,
    LEFT=8
};
enum Release{
	R_UP=16,
	R_RIGHT=32,
	R_DOWN=64,
	R_LEFT=128
};

struct Cell{
	Cell(int i, int j):i(i),j(j){}
	int i,j;
};

class Character : public Stroke{
private:

	void flag(int d);
	void unflag(int d);
	void clear(int d);
	bool moving();
	void accelerate(int d);
	void decelerate();
	int dmask;
    float speed;
protected:
    static YAML::Node file;
    const YAML::Node& node;

    glm::vec2 movement;
    glm::vec2 knockback;
    glm::mat4 matrix;

    void rotate_to(float degrees);
    void rotate(Direction direction);
public:
    Character(float x, float y, const YAML::Node& node);

    // Rendering
    Animations animations;
	Quad position;

    // Stats
    int health;

    // Combat
    bool stunned;
    float stun_timer;
    float stun_duration;
    float stun_multiplier;
    void harm(int damage);
    void stun(float duration);

    // Rendering Methods
	std::vector<float> vertices()const;
	std::vector<float> coordinates()const;
    void update();
    void animate(std::string animation);

    // Movement & Positioning
    float x();
    float y();
	void stop(int d);
	void go(int d);
    virtual void move(glm::vec2 velocity);
    glm::vec2 velocity();
	int direction();
	Cell facing();
    Cell cell();

    // Dev/debug crap
    void reload();

    // Static dev/debug crap
    static void reload_yaml();
};

#endif /* CHARACTER_H_ */
