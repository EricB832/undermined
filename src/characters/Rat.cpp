#include "Rat.h"

Rat::Rat(float x, float y):Enemy(x, y, "Rat"){
    health = 1;
    damage = 1;
    stun_length = 0.3f;
    knockback_strength = 200.f;
}
