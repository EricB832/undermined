#ifndef ENEMY_H
#define ENEMY_H

#include "Character.h"

class Enemy : public Character
{
public:
    Enemy(float x, float y, std::string name);

    //Stats
    int damage;
    float stun_length;
    float knockback_strength;
};

#endif // ENEMY_H
