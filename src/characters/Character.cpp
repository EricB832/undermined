/*
 * Character.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#include "../Game.h"
#include "Character.h"
#include "../floor/Square.h"
#include <yaml-cpp/yaml.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../rendering/Animations.h"

using namespace std;

const string path = "data/info/characters.yml";
YAML::Node Character::file = YAML::LoadFile(path);

Character::Character(float x, float y, const YAML::Node& node):
        dmask(240),
        node(node),
        movement(0,0),
        animations(node["Animations"], file["Width"].as<int>(), file["Height"].as<int>()),
        position(x, y, 0, 0),
        health(0),
        stunned(false),
        stun_timer(0),
        stun_duration(0),
        stun_multiplier(1){
    float w = node["Size"]["Width"].as<int>();
    float h = node["Size"]["Height"].as<int>();
    speed = node["Speed"].as<float>();
    position = Quad(x,y,w,h);

}

vector<float> combine(vector<float> v1, vector<float> v2) {
	vector<float> v;
	v.insert(v.end(), v1.begin(), v1.end());
	v.insert(v.end(), v2.begin(), v2.end());
	return v;
}

vector<float> Character::vertices() const{
    return position.pre_multiply(matrix).vertices();
}

vector<float> Character::coordinates() const{
    return animations.coordinates();
}

void Character::rotate(Direction direction){
    go(direction);
    stop(direction);
}

void Character::rotate_to(float degrees){
    matrix = glm::translate<GLfloat>(glm::mat4(), glm::vec3(position.center().x, position.center().y, 0));
    matrix = glm::rotate<GLfloat>(matrix, degrees, glm::vec3(0,0,-1));
    matrix = glm::translate<GLfloat>(matrix, glm::vec3(-position.center().x,-position.center().y,0));
}

void Character::stop(int d){
	flag(d<<4);
	unflag(d);
	if (!moving()){
		flag(d);
	}
}

void Character::go(int d){
	clear(~d & 0x0F);
	flag(d);
	unflag(d<<4);
}

void shrink(float& d, float v){
	if(d>0)d=(d-v)>0?d-v:0;
	if(d<0)d=(d+v)<0?d+v:0;
}

glm::vec2 Character::velocity(){
    return stunned
            ? knockback*Game::dt
            : (movement+knockback)*Game::dt;
}

void Character::accelerate(int d){
    auto theta = 3.1415926539/2. - (double)d*3.1415926539/4.;
	glm::vec2 acc(cos(theta),sin(theta));
    acc *= (speed/3.f);
    movement += acc;
    if (glm::length(movement) > speed){
        movement = glm::normalize(movement)*speed;
    }
}

void slow(glm::vec2& vec, float dec){
    if(glm::length(vec) == 0) return;
    vec = glm::length(vec - glm::normalize(vec) * dec) > glm::length(vec)
            ? glm::vec2(0,0)
            : vec - glm::normalize(vec) * dec;
}

void Character::animate(string animation){
    animations.play(animation);
}

void Character::update(){
    if(stunned){
        if(stun_timer > stun_duration){
            stunned = false;
            stun_timer = 0.f;
        }
        stun_timer += Game::dt;
    }
    if(moving()) {
        animate("Walking");
        accelerate(direction());
    } else {
        animate("Idle");
    }
    slow(movement, Game::dt*800.f);
    slow(knockback, Game::dt*800.f);
    move(velocity());
    rotate_to(45*direction());
    animations.animate(Game::dt);
}

void Character::move(glm::vec2 velocity){
    position.translate(velocity.x, velocity.y);
}

void Character::clear(int d){
	if ((d&UP) && (dmask & R_UP)) unflag(UP);
	if ((d&LEFT) && (dmask & R_LEFT)) unflag(LEFT);
	if ((d&DOWN) && (dmask & R_DOWN)) unflag(DOWN);
	if ((d&RIGHT) && (dmask & R_RIGHT)) unflag(RIGHT);
}

void Character::flag(int d){
	dmask |= d;
}

void Character::unflag(int d){
	dmask &= ~d;
}
bool Character::moving(){
    return (dmask & 0xF0) != 0xF0 && !stunned;
}

int Character::direction(){
	if((dmask & UP) && (dmask & RIGHT)) return 1;
	if((dmask & UP) && (dmask & LEFT)) return 7;
	if (dmask & UP) return 0;
	if((dmask & DOWN) && (dmask & RIGHT)) return 3;
	if((dmask & DOWN) && (dmask & LEFT)) return 5;
	if (dmask & DOWN) return 4;
	if (dmask & RIGHT) return 2;
	if (dmask & LEFT) return 6;
    return 0;
}

Cell Character::facing(){
	int d = direction();
	double e = 0.8;
    glm::vec2 p(0,0);
	switch(d){
    case 0: p = glm::vec2(position.center().x/Square::width, position.get(3).y/Square::height+e); break;
    case 1: p = glm::vec2(position.get(3).x/Square::width+e, position.get(3).y/Square::height+e); break;
    case 2: p = glm::vec2(position.get(3).x/Square::width+e, position.center().y/Square::height); break;
    case 3: p = glm::vec2(position.get(3).x/Square::width+e, position.get(0).y/Square::height-e); break;
    case 4: default: p = glm::vec2(position.center().x/Square::width, position.get(0).y/Square::height-e); break;
    case 5: p = glm::vec2(position.get(0).x/Square::width-e, position.get(0).y/Square::height-e); break;
    case 6: p = glm::vec2(position.get(0).x/Square::width-e, position.center().y/Square::height); break;
    case 7: p = glm::vec2(position.get(0).x/Square::width-e, position.get(3).y/Square::height+e); break;
	}
    return Cell(p.x>0?p.x:p.x-1, p.y>0?p.y:p.y-1);
}

Cell Character::cell(){
    glm::vec2 p(0,0);
    p = glm::vec2(position.center().x/Square::width, position.get(3).y/Square::height);
    return Cell(p.x>0?p.x:p.x-1, p.y>0?p.y:p.y-1);
}

void Character::stun(float duration){
    stunned = true;
    stun_duration = duration * stun_multiplier;
}

void Character::harm(int damage){
    health -= damage;
}

float Character::x(){
    return position.get(0).x;
}

float Character::y(){
    return position.get(0).y;
}

void Character::reload(){
    float w = node["Size"]["Width"].as<int>();
    float h = node["Size"]["Height"].as<int>();
    position = Quad(x(), y(), w, h);
}

void Character::reload_yaml(){
    file = YAML::LoadFile(path);
}
