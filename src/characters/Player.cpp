/*
 * Player.cpp
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#include "Player.h"
#include "../floor/Floor.h"
#include "../Game.h"
#include "../screens/Screen.h"
#include "Characters.h"
#include "Enemy.h"
#include "../items/Item.h"
#include <glm/glm.hpp>
#include <Gl/glfw3.h>
using namespace std;

Player::Player(float x, float y, std::string name, Floor& floor, Characters& characters, glm::mat4* camera):Character(x, y, file["Characters"][name]), floor(floor), characters(characters),weapon_x(0),weapon_y(0),camera(camera),weapon(nullptr) {
    auto node = file["Characters"][name];
    weapon_x = node["Weapon"]["Idle"]["0"]["X"].as<int>();
    weapon_y = node["Weapon"]["Idle"]["0"]["Y"].as<int>();
}

void Player::move(glm::vec2 velocity){
    Character::move(velocity);
    if(weapon){
        weapon->position.translate(velocity.x, velocity.y);
    }
}

void Player::update(){
    Character::update();
    if(weapon){
        weapon->rotate_about(position.center(), 45*direction());
    }
    collisions();
    offscreen();
}

char key(int direction){
	return 	direction == UP ? 'W' :
			direction == LEFT ? 'A' :
			direction == DOWN ? 'S' : 'D';

}

Control Player::movement_control(int direction) {
    return Control(key(direction), [this, direction](){
        this->go(direction);
    }, nullptr, [this, direction](){
        this->stop(direction);
    });
}

bool Player::dead(){
    return health <= 0;
}

void Player::collisions(){
    for(auto& wall : floor.walls){
        glm::vec2 resolution;
        if(position.collides(wall.quad(), &resolution)){
            move(resolution);
        }
    }
    for(auto& enemy : characters.enemies()){
        glm::vec2 resolution;
        if(position.collides(enemy->position, &resolution)){
            knockback = glm::normalize(resolution)*enemy->knockback_strength;
            move(resolution);
            harm(enemy->damage);
            stun(enemy->stun_length);
        }
    }
}

void Player::offscreen(){
    Quad screen_position = position.pre_multiply(*camera);

    if(screen_position.get(0).x > 1 ||
            screen_position.get(0).y > 1 ||
            screen_position.get(3).x < -1 ||
            screen_position.get(3).y < -1){

        dying = true;
    } else{
        dying = false;
    }

    if (dying){
        death_timer += Game::dt;
        if(death_timer > 1){
            death_timer -= 1;
            harm(1);
        }
    } else {
        death_timer = 0;
    }
}

void Player::reload(){
    Character::reload();
    weapon_x = node["Weapon"]["X"].as<int>();
    weapon_y = node["Weapon"]["Y"].as<int>();
    if(weapon){
        weapon->position = Quad(x() + weapon_x, y() + weapon_y, weapon->position.width(), weapon->position.height());
    }
}
