/*
 * Player.h
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "Character.h"
#include "../ui/Control.h"
#include <glm/glm.hpp>

class Floor;
class Characters;
class Item;

class Player: public Character {
private:
    void collisions();
    void offscreen();

    bool dying = false;
    float death_timer = 0;

    Floor& floor;
    Characters& characters;
protected:
    float weapon_x;
    float weapon_y;
public:
    Player(float x, float y, std::string name, Floor& floor, Characters& characters, glm::mat4* camera);
    void update();
    bool dead();
    glm::mat4* camera;
    Item* weapon;

    // Movement
    void move(glm::vec2 velocity);

    // Initialization
    virtual std::unique_ptr<Item> start_weapon()=0;
    Control movement_control(int direction);

    // Debug
    void reload();
};

#endif /* PLAYER_H_ */
