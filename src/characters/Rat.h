#ifndef RAT_H
#define RAT_H

#include "Enemy.h"

class Rat : public Enemy
{
public:
    Rat(float x, float y);
};

#endif // RAT_H
