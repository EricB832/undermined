/*
 * Characters.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#include <cpplinq.hpp>
#include "../floor/Square.h"
#include "Characters.h"
#include "Player.h"
#include "Enemy.h"

using namespace std;
using namespace cpplinq;

Characters::Characters():Painter("data/art/characters.png", "data/shaders/vertex.glsl", "data/shaders/fragment.glsl"){}

Player& Characters::player(){
    return dynamic_cast<Player&>(*strokes[0]);
}

vector<Enemy*> Characters::enemies() const {
    return from(strokes)
           >> select([](const unique_ptr<Stroke>& stroke){return dynamic_cast<Enemy*>(stroke.get());})
           >> where([](Enemy* enemy){return enemy;})
           >> to_vector();
}
