/*
 * Sid.h
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#ifndef SID_H_
#define SID_H_

#include "Player.h"
#include <glm/glm.hpp>

class Weapon;

class Knight : public Player{
public:
    Knight(float x, float y, Floor& floor, Characters& characters, glm::mat4* camera);
    std::unique_ptr<Item> start_weapon();
};

#endif /* SID_H_ */
