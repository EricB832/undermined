/*
 * Sid.cpp
 *
 *  Created on: Mar 15, 2013
 *      Author: Eric
 */

#include "Knight.h"
#include "../floor/Square.h"
#include "../items/Weapon.h"
#include <memory>
using namespace std;

Knight::Knight(float x, float y, Floor& floor, Characters& characters, glm::mat4* camera):
        Player(x, y, "Knight", floor, characters, camera){
    health = 3;
    rotate(UP);
}

unique_ptr<Item> Knight::start_weapon(){
    return unique_ptr<Item>(new Weapon(x() + weapon_x, y() + weapon_y, "Sword"));
}
