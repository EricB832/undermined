/*
 * GameScreen.h
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#ifndef GAMESCREEN_H_
#define GAMESCREEN_H_

#include "Screen.h"
#include "glm/glm.hpp"

class Game;
class Characters;
class Floor;

class GameScreen: public Screen {
private:
    Game& game;
    std::vector<Control> game_controls(Characters& characters);

public:
    GameScreen(Game& game);
	void print(){}
    void update();
    void reload();

    Floor& floor();
    Characters& characters();

    static glm::mat4 camera;
    bool paused;
};

#endif /* GAMESCREEN_H_ */
