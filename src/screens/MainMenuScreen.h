/*
 * MainMenu.h
 *
 *  Created on: Feb 22, 2013
 *      Author: eric
 */

#ifndef MAINMENU_H_
#define MAINMENU_H_

#include "Screen.h"
#include "../Game.h"
#include <glm/glm.hpp>

class MainMenuScreen: public Screen {
private:
    glm::mat4 camera;
public:
	MainMenuScreen(Game& game);
	void print();
	~MainMenuScreen(){}
};

#endif /* MAINMENU_H_ */

