#include "DeathScreen.h"
#include "../Options.h"
#include <string>

using namespace std;

DeathScreen::DeathScreen():font(new FTTextureFont(Options::menu_font().c_str())){
    font->FaceSize(72);
}

void DeathScreen::print(){
    FTBBox dimensions = font->BBox("YOU ARE DEAD");
    float x = (Screen::Width  - (dimensions.Upper().X() - dimensions.Lower().X()))/2;
    float y = (Screen::Height - (dimensions.Upper().Y() - dimensions.Lower().Y()))/2;

    glColor3d(0.7,0,0);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,Screen::Width,0,Screen::Height, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    font->Render("YOU ARE DEAD", -1, FTPoint(x,y));

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

void DeathScreen::update(){}
