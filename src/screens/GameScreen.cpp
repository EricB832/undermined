/*
 * GameScreen.cpp
 *
 *  Created on: Mar 6, 2013
 *      Author: eric
 */

#include "GameScreen.h"
#include "DeathScreen.h"
#include "../Options.h"
#include "../floor/Floor.h"
#include "../characters/Characters.h"
#include "../ui/Hud.h"
#include "../items/Items.h"
#include "../items/Item.h"
#include "../Error.h"
#include "../Game.h"
#include "../characters/Rat.h"
#include "../characters/Knight.h"
#include "../ui/Backdrop.h"
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <string>
#include <cstdlib>
#include <cpplinq.hpp>
using namespace std;

vector<Control> game_controls(Characters& characters);

glm::mat4 GameScreen::camera = glm::mat4();

GameScreen::GameScreen(Game& game):game(game), paused(false) {
    painters.emplace_back(new Backdrop("data/art/mines.png"));
    Backdrop& backdrop = static_cast<Backdrop&>(*painters.back());

    painters.emplace_back(new Floor(5,150));
    Floor& floor = static_cast<Floor&>(*painters.back());

    float w = floor.width*Square::width;
    camera = glm::translate<GLfloat>(glm::ortho<GLfloat>(0, Width, 0, Height), glm::vec3((Width-w)/2, Height/2, 0));

    painters.emplace_back(new Characters);
    Characters& characters 	= static_cast<Characters&>(*painters.back());
    characters.strokes.emplace_back(new Knight(128, 0, floor, characters, &camera));
    characters.strokes.emplace_back(new Rat(128, 100));

    painters.emplace_back(new Hud(characters.player()));
    Hud& hud = static_cast<Hud&>(*painters.back());

    painters.emplace_back(new Items);
    Items& items = static_cast<Items&>(*painters.back());
    items.strokes.push_back(move(characters.player().start_weapon()));

    characters.player().weapon = static_cast<Item*>(items.strokes.back().get());

    controls.push(game_controls(characters));

	for(auto& painter : painters){
        painter->matrix = &camera;
	}
    backdrop.matrix = hud.matrix = &Hud::Camera;

}

vector<Control> GameScreen::game_controls(Characters& characters){
	Player& player = characters.player();
	vector<Control> controls;
    controls.emplace_back(player.movement_control(UP));
    controls.emplace_back(player.movement_control(LEFT));
    controls.emplace_back(player.movement_control(DOWN));
    controls.emplace_back(player.movement_control(RIGHT));
    controls.emplace_back(Control('P', [this](){this->paused = !this->paused;}));
    controls.emplace_back(Control('R', [this](){this->reload();}));
	return controls;
}

void GameScreen::reload(){
    // Definitely not platform independent
    if(system(nullptr)){
        int s = system("..\\Undermined\\postbuild.bat");
        cout << s << endl;
    }
    Character::reload_yaml();
    Item::reload_yaml();
    for (auto& painter : painters){
        for (auto& stroke : painter->strokes){
            stroke->reload();
        }
    }
}

Floor& GameScreen::floor(){
    using namespace cpplinq;
    return *(from(painters)
            >> select([](const unique_ptr<Painter>& painter){return dynamic_cast<Floor*>(painter.get());})
            >> where ([](const Floor* floor){return floor;})
            >> first());
}

Characters& GameScreen::characters(){
    using namespace cpplinq;
    return *(from(painters)
            >> select([](const unique_ptr<Painter>& painter){return dynamic_cast<Characters*>(painter.get());})
            >> where([](const Characters* characters){return characters;})
            >> first());
}

void GameScreen::update(){
    if (paused){

    } else {
        camera = glm::translate(camera, glm::vec3(0, -floor().scroll_speed*Game::dt, 0));
        if(characters().player().dead()){
            game.transition(new DeathScreen);
        }
    }
    Screen::update();
}
