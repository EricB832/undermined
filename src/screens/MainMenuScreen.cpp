/*
 * MainMenu.cpp
 *
 *  Created on: Feb 22, 2013
 *      Author: eric
 */

#include "MainMenuScreen.h"
#include "../ui/Button.h"
#include "../ui/Menu.h"
#include "GameScreen.h"
#include "../ui/Backdrop.h"
#include "../Game.h"
#include <memory>
#include <Gl/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
using namespace std;

vector<Control> main_menu_controls(Menu& menu);
vector<unique_ptr<Stroke>> main_menu_buttons(Game& game, Menu& menu);

MainMenuScreen::MainMenuScreen(Game& game){
    painters.emplace_back(new Backdrop("data/art/Undermined.png"));
    painters.emplace_back(new Menu);
    Menu& menu = static_cast<Menu&>(*painters[1]);

	controls.push(main_menu_controls(menu));
    menu.strokes = main_menu_buttons(game, menu);

	menu.set(0);

    camera = glm::ortho(0.f, Width, 0.f, Height);

    for(auto& painter : painters){
        painter->matrix = &camera;
    }
}

void MainMenuScreen::print(){
    ((Menu&)*painters[1]).print();
}

vector<unique_ptr<Stroke>> main_menu_buttons(Game& game, Menu& menu){
	vector<unique_ptr<Stroke>> buttons;

	buttons.emplace_back(new Button("Start", *menu.font,
		[&game](){
            game.transition(new GameScreen(game));
		}));

	buttons.emplace_back(new Button("Exit" , *menu.font,
		[](){
            Game::exit(Game::window);
		}));

	return buttons;
}

vector<Control> main_menu_controls(Menu& menu){
	vector<Control> controls;
	controls.emplace_back(GLFW_KEY_DOWN,  [&menu](){menu.down();}, nullptr, nullptr);
	controls.emplace_back(GLFW_KEY_UP,    [&menu](){menu.up();}, nullptr, nullptr);
	controls.emplace_back(GLFW_KEY_ENTER, [&menu](){menu.press();}, nullptr, [&menu](){menu.release(); menu.activate();});
	return controls;
}
