#ifndef DEATHSCREEN_H
#define DEATHSCREEN_H

#include "Screen.h"
#include <FTGL/ftgl.h>

class DeathScreen : public Screen {
private:
    std::unique_ptr<FTFont> font;

public:
    DeathScreen();
    void update();
    void print();
};

#endif // DEATHSCREEN_H
