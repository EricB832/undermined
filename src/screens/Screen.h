/*
 * Screen.h
 *
 *  Created on: Feb 22, 2013
 *      Author: eric
 */

#ifndef SCREEN_H_
#define SCREEN_H_

#include "../rendering/Painter.h"
#include "../ui/Control.h"
#include <glm/glm.hpp>
#include <stack>
class Screen {
public:
    static float Width;
    static float Height;

    virtual void update();
	void render();
	virtual void print()=0;
	void clear();

	std::vector<std::unique_ptr<Painter>> painters;
	std::stack<std::vector<Control>> controls;
};

#endif /* SCREEN_H_ */
