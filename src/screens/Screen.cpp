/*
 * Screen.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */
#include "Screen.h"

float Screen::Width = 1200;
float Screen::Height = 900;

void Screen::update() {
	for(auto control : controls.top()){
		control.handle();
	}
    for(auto& painter : painters){
        painter->update();
    }
}
void Screen::render() {
    for(auto& painter : painters){
        painter->prepare();
	}
    for(auto& painter : painters){
        painter->render();
    }
	print();
}
void Screen::clear(){
	for(auto& painter : painters){
		painter->clear();
	}
}
