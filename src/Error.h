/*
 * Error.h
 *
 *  Created on: Feb 20, 2013
 *      Author: eric
 */

#ifndef ERROR_H_
#define ERROR_H_

#include <GL/glew.h>

class Error {
public:
	static void print(GLenum error);
	static void printAll();
};

#endif /* ERROR_H_ */
