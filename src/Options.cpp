#include <yaml-cpp/yaml.h>
#include "Options.h"

using namespace std;

const YAML::Node Options::file = YAML::LoadFile("data/init.yml");

int Options::width(){
    return file["Width"].as<int>();
}

int Options::height(){
    return file["Height"].as<int>();
}

string Options::menu_font(){
    return file["Menu Font"].as<string>();
}
