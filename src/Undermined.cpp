#define GLEW_STATIC

#include <iostream>
#include <GL/glew.h>
#include <Gl/glfw3.h>
#include "Game.h"

using namespace std;

int main(){
	// Init
	glfwInit();
	Game game;
	game.open();
    glewInit();
	// Callbacks
    glfwSetWindowCloseCallback(Game::window, Game::exit);
    // Setup
	game.setup();
	// Game loop
    while(!glfwWindowShouldClose(game.window)){
		game.run();
	}
	glfwTerminate();
}

