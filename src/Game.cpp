#include <fstream>
#include <iostream>
#include <memory>
#include "Game.h"
#include "Error.h"
#include "screens/MainMenuScreen.h"
#include "Options.h"
#include <Gl/glfw3.h>
#include <iostream>
using namespace std;

float Game::dt = 16.f;
GLFWwindow* Game::window = nullptr;

void Game::open(){
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    window = glfwCreateWindow(Options::width(), Options::height(), "Undermined", nullptr, nullptr);
    glfwMakeContextCurrent(window);
}

void Game::setup(){
	screen = std::unique_ptr<Screen>(new MainMenuScreen(*this));
	next = nullptr;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Game::update(){
	screen->update();
}

void Game::render(){
	glClear(GL_COLOR_BUFFER_BIT);

	screen->render();

    glfwSwapBuffers(window);
    glfwPollEvents();
}

void Game::run(){
	double d = glfwGetTime();
	update();
	render();
	dt = glfwGetTime() - d;
	if(next){
		screen->clear();
		screen.reset(next.release());
		next = nullptr;
	}
}

void Game::transition(Screen* screen){
    next.reset(screen);
}

void Game::exit(GLFWwindow* window){
    glfwDestroyWindow(window);
    std::exit(0);
}
