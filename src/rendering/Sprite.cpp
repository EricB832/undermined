/*
 * Sprite.cpp
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#include "Sprite.h"
#include "Stroke.h"
#include <yaml-cpp/yaml.h>
using namespace std;

Sprite::Sprite(const YAML::Node& node, float w, float h){
    x = node["S"].as<int>()/w;
    y = node["T"].as<int>()/h;
    width  = node["DS"].as<int>()/w;
    height = node["DT"].as<int>()/h;
}

std::vector<GLfloat> Sprite::coordinates() const{
    return {x,y,x+width,y,x,y+height,x+width,y,x+width,y+height,x,y+height};
}
