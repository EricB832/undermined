/*
 * Sprite.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef SPRITE_H_
#define SPRITE_H_

#include <string>
#include <vector>
#include <GL/glew.h>
#include <yaml-cpp/yaml.h>
#include <glm/glm.hpp>

class Sprite {
private:
    float x, y, width, height;
public:
    Sprite(const YAML::Node& node, float w, float h);
	std::vector<GLfloat> coordinates() const;
};

#endif /* SPRITE_H_ */
