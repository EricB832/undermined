/*
 * Painter.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#include "Painter.h"
#include "../Error.h"
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <SOIL/SOIL.h>
#include <string>
#include <iostream>
#include <vector>
using namespace std;
const std::string Painter::UNIFORM_MATRIX = "matrix";
const std::string Painter::UNIFORM_TEXTURE = "texture";
const std::string Painter::ATTRIBUTE_XY = "xy";
const std::string Painter::ATTRIBUTE_ST = "st";

Painter::Painter(std::string texture, std::string vertex, std::string fragment):texture(texture),vertex(vertex),fragment(fragment),
        matrix(nullptr){
    buildShader(vertex,fragment);
    buildTexture(texture);
}

void Painter::getIDs(){
    attributeVertices = glGetAttribLocation(shader.program, ATTRIBUTE_XY.c_str());
    attributeCoordinates = glGetAttribLocation(shader.program, ATTRIBUTE_ST.c_str());
    uniformMatrix = glGetUniformLocation(shader.program, UNIFORM_MATRIX.c_str());
    uniformTexture = glGetUniformLocation(shader.program, UNIFORM_TEXTURE.c_str());
	Error::printAll();
}

GLuint getBuffer(GLenum target, const void* data, GLsizei size){
    GLuint b;
    glGenBuffers(1, &b);
    glBindBuffer(target, b);
	glBufferData(target, size, data, GL_STATIC_DRAW);
	Error::printAll();
    return b;
}

void Painter::buildBuffers(vector<GLfloat> vertices, vector<GLfloat> coordinates, vector<GLuint> elements){
	bufferVertices = getBuffer(GL_ARRAY_BUFFER, vertices.data(), (GLsizeiptr)(vertices.size()*sizeof(GLfloat)));
	bufferCoordinates = getBuffer(GL_ARRAY_BUFFER, coordinates.data(), (GLsizeiptr)(coordinates.size()*sizeof(GLfloat)));
	bufferElements = getBuffer(GL_ELEMENT_ARRAY_BUFFER, elements.data(), (GLsizeiptr)(elements.size()*sizeof(GLuint)));
	Error::printAll();
}

void Painter::buildTexture(std::string tex){
	texID = SOIL_load_OGL_texture(tex.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
	Error::printAll();
}

void Painter::buildShader(std::string vertex, std::string fragment){
	shader.buildShader(vertex, fragment);
	Error::printAll();
}

void Painter::prepare() {
    if(strokes.empty()) return;
    buildBuffers(vertices(), coordinates(), elements());
    getIDs();
}

void Painter::render() const{
    if(strokes.empty()) return;
    glUseProgram(shader.program);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);
	glUniform1i(uniformTexture, 0);
    glUniformMatrix4fv(uniformMatrix, 1, GL_FALSE, glm::value_ptr(matrix?*matrix:glm::mat4()));

	glBindBuffer(GL_ARRAY_BUFFER, bufferCoordinates);
	glVertexAttribPointer(attributeCoordinates, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*2, nullptr);
	glEnableVertexAttribArray(attributeCoordinates);

	glBindBuffer(GL_ARRAY_BUFFER, bufferVertices);
	glVertexAttribPointer(attributeVertices, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*2, nullptr);
	glEnableVertexAttribArray(attributeVertices);

	//Make magic!
	GLsizei size;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferElements);
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
	glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, nullptr);

	//Clean up
	glDisableVertexAttribArray(attributeCoordinates);
	glDisableVertexAttribArray(attributeVertices);
	glUseProgram(0);

	// Make sure nothing went wrong
	Error::printAll();
}

void Painter::update(){
    for (auto& stroke : strokes){
        stroke->update();
    }
}

vector<GLfloat> Painter::vertices() const{
	if (strokes.empty()) return vector<GLfloat>();
	vector<GLfloat> vertices;
	for(auto p = strokes.begin(); p != strokes.end(); p++){
		vector<GLfloat> v = (*p)->vertices();
		vertices.insert(vertices.end(), v.begin(), v.end());
	}
	return vertices;
}
vector<GLfloat> Painter::coordinates()const{
	if (strokes.empty()) return vector<GLfloat>();
	vector<GLfloat> coordinates;
    for(auto p = strokes.begin(); p != strokes.end(); p++){
		vector<GLfloat> c = (*p)->coordinates();
		coordinates.insert(coordinates.end(), c.begin(), c.end());
	}
	return coordinates;
}

vector<GLuint>  Painter::elements() const{
	vector<GLuint> elements;
	for(auto p = strokes.begin(); p != strokes.end(); p++){
		vector<GLfloat> v = (*p)->vertices();
		for(unsigned i = 0; i < v.size(); i+=2){
			elements.push_back(elements.size());
		}
	}
	return elements;
}

void Painter::clear(){
    glDeleteTextures(1, &texID);
    strokes.clear();
    shader.clear();
}


