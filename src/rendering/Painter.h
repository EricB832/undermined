/*
 * Painter.h
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#ifndef PAINTER_H_
#define PAINTER_H_

#include <vector>
#include <string>
#include <map>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <memory>
#include "../ui/Control.h"
#include "Stroke.h"
#include "Shader.h"

class Painter {
public:
    // Constructors
	Painter(std::string texture, std::string vertex, std::string fragment);
	Painter(const Painter&)=delete;
	Painter(Painter&&)=default;

    // Painting
    void prepare();
    void render() const;

    // Updating
    virtual void update();
    void clear();

    // Properties
	std::vector<GLfloat> vertices()    const;
	std::vector<GLfloat> coordinates() const;
	std::vector<GLuint>  elements()    const;
	std::string texture;
	std::string vertex;
	std::string fragment;

    std::vector<std::unique_ptr<Stroke> > strokes;
    glm::mat4* matrix;
private:
	static const std::string UNIFORM_MATRIX, UNIFORM_TEXTURE, ATTRIBUTE_XY, ATTRIBUTE_ST;
	void getIDs();
    void buildTexture(std::string tex);
    void buildShader(std::string vertex, std::string fragment);
    void buildBuffers(std::vector<GLfloat> vertices, std::vector<GLfloat> coordinates, std::vector<GLuint> elements);

    void updateSubTextureBuffer(const std::vector<GLfloat>& c, GLuint index);
    void updateSubVertexBuffer(const std::vector<GLfloat>& v, GLuint index);

	GLuint texID;

	GLuint bufferVertices;
	GLuint bufferCoordinates;
	GLuint bufferElements;

	GLint attributeVertices;
	GLint attributeCoordinates;

	GLint uniformTexture;
	GLint uniformMatrix;

	Shader shader;
};

#endif /* PAINTER_H_ */
