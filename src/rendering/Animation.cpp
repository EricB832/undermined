#include "Animation.h"
#include "Stroke.h"
using namespace std;

Animation::Animation(const YAML::Node& node, float w, float h){
    for(auto frame : node){
        frames.emplace_back(frame["Duration"].as<float>(), Sprite(frame["Texture"], w, h));
    }
    frame = 0;
    frame_timer = 0;
}

vector<float> Animation::coordinates() const{
    return frames[frame].second.coordinates();
}

void Animation::animate(float time){
    if(frames[frame].first == 0) return;
    frame_timer += time;
    if(frame_timer > frames[frame].first){
        frame_timer -= frames[frame].first;
        frame++;
        if(frame >= frames.size()){
            frame = 0;
        }
    }
}
