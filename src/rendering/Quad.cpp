/*
 * Quad.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */

#include "Quad.h"
#include "../Options.h"
#include "Stroke.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
using namespace std;

const string Quad::QUAD = "Position";
const string Quad::X = "X";
const string Quad::Y = "Y";
const string Quad::WIDTH = "Width";
const string Quad::HEIGHT = "Height";

Quad::Quad(float x, float y, float w, float h): p1(x,y),p2(x+w,y),p3(x,y+h),p4(x+w,y+h) {}

Quad::Quad(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 p4): p1(p1), p2(p2), p3(p3), p4(p4){}

Quad::Quad(const YAML::Node& node){
    float x,y,w,h;
    x = node["X"].as<float>();
    y = node["Y"].as<float>();
    w = node["Width"].as<float>();
    h = node["Height"].as<float>();
    p1 = glm::vec2(x,y);
    p2 = glm::vec2(x+w,y);
    p3 = glm::vec2(x, y+h);
    p4 = glm::vec2(x+w, y+h);
}


vector<GLfloat> Quad::vertices() const{
	if(p1.x==p4.x && p1.y == p4.y && p1.x==0 && p1.y==0) return vector<GLfloat>();
    return {p1.x,p1.y,p2.x,p2.y,p3.x,p3.y,p2.x,p2.y,p4.x,p4.y,p3.x,p3.y};
}

float Quad::width() const{
	return p4.x-p1.x;
}

float Quad::height() const{
	return p4.y-p1.y;
}

glm::vec2 Quad::get(unsigned i) const{
	switch(i){
	case 1: return p2;
	case 2: return p3;
	case 3: return p4;
	case 0: default: return p1;
	}
}

glm::vec2 Quad::center(){
    return glm::vec2((p1.x+p2.x+p3.x+p4.x)/4, (p1.y+p2.y+p3.y+p4.y)/4);
}

glm::vec2 multiply(glm::mat4 matrix, glm::vec2 vec) {
    glm::vec4 p = glm::vec4(vec.x,vec.y,0,1);
    glm::vec4 q = matrix*p;
    return glm::vec2(q.x/q.w, q.y/q.w);
}

Quad Quad::pre_multiply(glm::mat4 matrix) const{
    glm::vec2 q1 = multiply(matrix, p1);
    glm::vec2 q2 = multiply(matrix, p2);
    glm::vec2 q3 = multiply(matrix, p3);
    glm::vec2 q4 = multiply(matrix, p4);
    return Quad(q1, q2, q3, q4);
}

void Quad::translate(float x, float y){
    p1 = glm::vec2(p1.x + x, p1.y + y);
    p2 = glm::vec2(p2.x + x, p2.y + y);
    p3 = glm::vec2(p3.x + x, p3.y + y);
    p4 = glm::vec2(p4.x + x, p4.y + y);
}
void Quad::move(float x, float y){
	float w = width();
	float h = height();
    p1 = glm::vec2(x,y);
    p2 = glm::vec2(x+w,y);
    p3 = glm::vec2(x, y+h);
    p4 = glm::vec2(x+w, y+h);
}

Quad Quad::shrink(float margin) {
    double pixw = 2./Options::width();
    double pixh = 2./Options::height();
	return Quad(p1.x+margin*pixw, p1.y+margin*pixh, width()-2*margin*pixw, height()-2*margin*pixh);
}

bool Quad::collides(Quad quad, glm::vec2* resolution){
    glm::vec2 qp1 = quad.get(0);
    glm::vec2 qp4 = quad.get(3);

    if(p4.x < qp1.x ||
       p4.y < qp1.y ||
       p1.x > qp4.x ||
       p1.y > qp4.y){
        return false;
    }

    float left = (qp1.x - p4.x);
    float right = (qp4.x - p1.x);
    float top = (qp1.y - p4.y);
    float bottom = (qp4.y - p1.y);

    resolution->x = (abs(left) < abs(right)) ? left : right;
    resolution->y = (abs(top) < abs(bottom)) ? top : bottom;
    if(abs(resolution->x) < abs(resolution->y)){
        resolution->y = 0;
    } else {
        resolution->x = 0;
    }

    return true;
}
