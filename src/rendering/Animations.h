#ifndef ANIMATIONS_H
#define ANIMATIONS_H

#include "Animation.h"
#include <string>
#include <map>
#include <yaml-cpp/yaml.h>

class Animations
{
protected:
    std::string animation;
    std::map<std::string, Animation> animations;
public:
    Animations(std::string animation, const YAML::Node& node, float w, float h);
    Animations(const YAML::Node& node, float w, float h);
    std::vector<float> coordinates() const;
    void play(std::string animation);
    void animate(float time);
};

#endif // ANIMATIONS_H
