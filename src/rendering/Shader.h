/*
 * Shader.h
 *
 *  Created on: Feb 19, 2013
 *      Author: eric
 */

#ifndef SHADER_H_
#define SHADER_H_

#include <string>
#include <vector>
#include <map>
#include <GL/glew.h>

class Shader {
public:
    // Constructors
    Shader()=default;
	Shader(const Shader&)=delete;
	Shader(Shader&&)=default;

    // Methods
	void buildShader(std::string vertex, std::string fragment);
    void clear();

    // Members
    GLuint program;
};

#endif /* SHADER_H_ */
