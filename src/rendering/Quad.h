/*
 * Quad.h
 *
 *  Created on: Mar 9, 2013
 *      Author: eric
 */

#ifndef QUAD_H_
#define QUAD_H_

#include <glm/glm.hpp>
#include <vector>
#include <GL/glew.h>
#include <yaml-cpp/yaml.h>

class Quad {
private:
	static const std::string QUAD, X, Y, WIDTH, HEIGHT;
    glm::vec2 p1,p2,p3,p4;
public:
	Quad(float x, float y, float w, float h);
    Quad(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 p4);
    Quad(const YAML::Node& node);

    bool collides(Quad quad, glm::vec2* resolution);
    Quad pre_multiply(glm::mat4 matrix) const;
	void translate(float x, float y);
	void move(float x, float y);
	std::vector<GLfloat> vertices() const;
	float width() const;
	float height() const;
    glm::vec2 get(unsigned i) const;
    glm::vec2 center();
	Quad shrink (float margin);
};

#endif /* QUAD_H_ */
