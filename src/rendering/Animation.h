#ifndef ANIMATION_H
#define ANIMATION_H

#include "Sprite.h"
#include <vector>
#include <string>
#include <map>

class Animation
{
private:
    unsigned frame;
    float frame_timer;
public:
    Animation(const YAML::Node& node, float w, float h);

    std::vector<std::pair<float, Sprite>> frames;
    std::vector<float> coordinates() const;
    void animate(float time);
};

#endif // ANIMATION_H
