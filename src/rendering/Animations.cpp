#include "Animations.h"
using namespace std;

Animations::Animations(string animation, const YAML::Node& node, float w, float h):animation(animation){
    for (auto a : node){
        animations.insert(map<string,Animation>::value_type(a.first.as<string>(), Animation(a.second, w, h)));
    }
}

Animations::Animations(const YAML::Node& node, float w, float h):Animations("Idle", node, w, h){

}

vector<float> Animations::coordinates() const{
    return animations.find(animation)->second.coordinates();
}

void Animations::play(string animation){
    this->animation = animation;
}

void Animations::animate(float time){
    animations.find(animation)->second.animate(time);
}
