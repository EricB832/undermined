/*
 * Stroke.h
 *
 *  Created on: Mar 8, 2013
 *      Author: eric
 */

#ifndef STROKE_H_
#define STROKE_H_

#include <vector>
#include <string>
#include <yaml-cpp/yaml.h>

class Stroke {
public:
	virtual std::vector<float> vertices   ()const=0;
	virtual std::vector<float> coordinates()const=0;
    virtual void update();
    virtual void reload();

    static const YAML::Node& get_node(const YAML::Node& file, std::vector<std::string> name);
};

#endif /* STROKE_H_ */
