/*
 * Options.h
 *
 *  Created on: Feb 18, 2013
 *      Author: eric
 */

#ifndef OPTIONS_H_
#define OPTIONS_H_

#include <map>
#include <string>
#include <vector>
#include <map>
#include <yaml-cpp/yaml.h>

class Options {
private:
    static const YAML::Node file;
public:
    static int width();
    static int height();
    static std::string menu_font();
};

#endif /* OPTIONS_H_ */
