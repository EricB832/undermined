TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/Undermined.cpp \
    src/Game.cpp \
    src/Error.cpp \
    src/Options.cpp \
    src/ui/Menu.cpp \
    src/ui/Keys.cpp \
    src/ui/Element.cpp \
    src/ui/Control.cpp \
    src/ui/Button.cpp \
    src/floor/Tile.cpp \
    src/floor/Square.cpp \
    src/items/Items.cpp \
    src/items/Item.cpp \
    src/floor/Floor.cpp \
    src/floor/Furniture.cpp \
    src/screens/Screen.cpp \
    src/screens/MainMenuScreen.cpp \
    src/screens/GameScreen.cpp \
    src/rendering/Sprite.cpp \
    src/rendering/Shader.cpp \
    src/rendering/Quad.cpp \
    src/rendering/Painter.cpp \
    src/characters/Player.cpp \
    src/characters/Characters.cpp \
    src/characters/Character.cpp \
    src/floor/Wall.cpp \
    src/ui/Hud.cpp \
    src/ui/HudElement.cpp \
    src/screens/DeathScreen.cpp \
    src/characters/Knight.cpp \
    src/characters/Rat.cpp \
    src/ui/Backdrop.cpp \
    src/ui/Background.cpp \
    src/characters/Enemy.cpp \
    src/rendering/Stroke.cpp \
    src/rendering/Animation.cpp \
    src/rendering/Animations.cpp \
    src/items/Weapon.cpp

HEADERS += \
    src/Options.h \
    src/Game.h \
    src/Error.h \
    src/ui/Menu.h \
    src/ui/Keys.h \
    src/ui/Element.h \
    src/ui/Control.h \
    src/ui/Button.h \
    src/floor/Tile.h \
    src/floor/Square.h \
    src/items/Items.h \
    src/items/Item.h \
    src/floor/Floor.h \
    src/floor/Furniture.h \
    src/screens/Screen.h \
    src/screens/MainMenuScreen.h \
    src/screens/GameScreen.h \
    src/rendering/Stroke.h \
    src/rendering/Sprite.h \
    src/rendering/Shader.h \
    src/rendering/Quad.h \
    src/rendering/Painter.h \
    src/characters/Player.h \
    src/characters/Characters.h \
    src/characters/Character.h \
    src/floor/Wall.h \
    src/ui/Hud.h \
    src/ui/HudElement.h \
    src/screens/DeathScreen.h \
    src/characters/Knight.h \
    src/characters/Rat.h \
    src/ui/Backdrop.h \
    src/ui/Background.h \
    src/characters/Enemy.h \
    src/rendering/Animation.h \
    src/rendering/Animations.h \
    src/items/Weapon.h

LIBS += -L../undermined/lib -lglfw3 -lglew32 -lSOIL -lFTGL -lopengl32 -lglu32 -lgdi32 -lfreetype  -lyaml-cpp

INCLUDEPATH += include
QMAKE_CXXFLAGS += -std=c++11 -isystem "../undermined/include"


OTHER_FILES +=
